
# Init Alice, Bob and Carol accounts

**[Rippled installation](001-InstallationRippled.md)**

### Run standalone
```bash
sudo rippled -a --start
```

### Current ledger
```bash
rippled ledger current
```

### Advance ledger
```bash
rippled ledger_accept
```

### Hardcoded account
```bash
ISSUER_ADDRESS=rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh && \
ISSUER_SECRET=snoPBrXtMeMyMHUVTgbuqAfg1SUTb && \
rippled account_info $ISSUER_ADDRESS
```

### Generate Alice account
```bash
rippled wallet_propose > alice.json && \
ALICE_ACCOUNT=$(cat alice.json | jq .result.account_id | sed 's/\"//g') && echo ALICE_ACCOUNT: $ALICE_ACCOUNT && \
ALICE_SECRET=$(cat alice.json | jq .result.master_seed | sed 's/\"//g') && echo ALICE_SECRET: $ALICE_SECRET
```

### Sign and send trasfer from initial account
```bash
SIGNED_BLOB=$(rippled sign $ISSUER_SECRET '{"TransactionType": "Payment", "Account": "'$ISSUER_ADDRESS'", "Destination": "'$ALICE_ACCOUNT'", "Amount": "1000000000", "Sequence": 1, "Fee": "10"}' offline | jq .result.tx_blob  | sed 's/\"//g') && \
rippled submit $SIGNED_BLOB
```

### Alice account info
```bash
rippled account_info $ALICE_ACCOUNT
```

### Generate Bob account
```bash
rippled wallet_propose > bob.json && \
BOB_ACCOUNT=$(cat bob.json | jq .result.account_id | sed 's/\"//g') && echo BOB_ACCOUNT: $BOB_ACCOUNT && \
BOB_SECRET=$(cat bob.json | jq .result.master_seed | sed 's/\"//g') && echo BOB_SECRET: $BOB_SECRET
```

### Sign and send trasfer from initial account (Sequence incremented)
```bash
SIGNED_BLOB=$(rippled sign $ISSUER_SECRET '{"TransactionType": "Payment", "Account": "'$ISSUER_ADDRESS'", "Destination": "'$BOB_ACCOUNT'", "Amount": "1000000000", "Sequence": 2, "Fee": "10"}' offline | jq .result.tx_blob  | sed 's/\"//g') && \
rippled submit $SIGNED_BLOB
```

### Bob account info
```bash
rippled account_info $BOB_ACCOUNT
```

### Generate Carol account
```bash
rippled wallet_propose > carol.json && \
CAROL_ACCOUNT=$(cat carol.json | jq .result.account_id | sed 's/\"//g') && echo CAROL_ACCOUNT: $CAROL_ACCOUNT && \
CAROL_SECRET=$(cat carol.json | jq .result.master_seed | sed 's/\"//g') && echo CAROL_SECRET: $CAROL_SECRET
```

### Sign and send trasfer from initial account (Sequence incremented)
```bash
SIGNED_BLOB=$(rippled sign $ISSUER_SECRET '{"TransactionType": "Payment", "Account": "'$ISSUER_ADDRESS'", "Destination": "'$CAROL_ACCOUNT'", "Amount": "1000000000", "Sequence": 3, "Fee": "10"}' offline | jq .result.tx_blob  | sed 's/\"//g') && \
rippled submit $SIGNED_BLOB
```

### Carol account info
```bash
rippled account_info $CAROL_ACCOUNT
```

### Sign and send trasfer from Alice to Bob
```bash
SIGNED_BLOB=$(rippled sign $ALICE_SECRET '{"TransactionType": "Payment", "Account": "'$ALICE_ACCOUNT'", "Destination": "'$BOB_ACCOUNT'", "Amount": "10000000", "Sequence": 1, "Fee": "10"}' offline | jq .result.tx_blob  | sed 's/\"//g') && \
rippled submit $SIGNED_BLOB
```

### Accounts info
```bash
rippled account_info $ALICE_ACCOUNT && \
rippled account_info $BOB_ACCOUNT && \
rippled account_info $CAROL_ACCOUNT && \
rippled account_info $ISSUER_ADDRESS
```