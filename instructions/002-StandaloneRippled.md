# Create account and fill

**[Rippled installation](001-InstallationRippled.md)**

### Run standalone
```bash
sudo rippled -a --start
```

### Current ledger
```bash
rippled ledger current
```

### Advance ledger
```bash
rippled ledger_accept
```

### Check cerrent ledger again
```bash
rippled ledger current
```

### Generate account
```bash
rippled wallet_propose > test-account.json &&
ACCOUNT=$(cat test-account.json | jq .result.account_id | sed 's/\"//g') && echo ACCOUNT: $ACCOUNT &&
SECRET=$(cat test-account.json | jq .result.master_seed | sed 's/\"//g') && echo SECRET: $SECRET
```

### Hardcoded account info
```bash
rippled account_info rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh
```

### New account info (there is no such yet)
```bash
rippled account_info $ACCOUNT
```

### Sign trasfer from initial account
```bash
SIGNED_BLOB=$(rippled sign snoPBrXtMeMyMHUVTgbuqAfg1SUTb '{"TransactionType": "Payment", "Account": "rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh", "Destination": "'$ACCOUNT'", "Amount": "2000000000", "Sequence": 1, "Fee": "10000"}' offline | jq .result.tx_blob  | sed 's/\"//g') && echo $SIGNED_BLOB
```

### Send trasfer from initial account
```bash
rippled submit $SIGNED_BLOB
```

### Hardcoded account info
```bash
rippled account_info rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh
```

### New account info (there is no such yet)
```bash
rippled account_info $ACCOUNT
```

### Sign trasfer from our account
```bash
SIGNED_BLOB=$(rippled sign $SECRET '{"TransactionType": "Payment", "Account": "'$ACCOUNT'", "Destination": "rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh", "Amount": "1000000000", "Sequence": 1, "Fee": "10000"}' offline | jq .result.tx_blob  | sed 's/\"//g') && echo $SIGNED_BLOB
```

### Send trasfer from our account
```bash
rippled submit $SIGNED_BLOB
```

### Hardcoded account info
```bash
rippled account_info rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh
```

### New account is created
```bash
rippled account_info $ACCOUNT
```
