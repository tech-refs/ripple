
# Multisignature account

**[Setup standalone Alice $ Co](003-Init-Alice,-Bob-and-Carol-accounts.md)**

We will create miltisignature account that is ruled by Alice with Bob and then send coins from it to Carol

### Accepting transactions made before

```bash
rippled ledger_accept
```

### Check Carol balance befere sending multisignature transaction

```bash
CAROL_INFO=$(rippled account_info $CAROL_ACCOUNT) && \
echo CAROL_INFO: $CAROL_INFO && \
echo CAROL BALANCE: $(echo $CAROL_INFO | jq .result.account_data.Balance)
```

### New account for setting-up multisigning

```bash
rippled wallet_propose > multi.json && \
MULTI_ACCOUNT=$(cat multi.json | jq .result.account_id | sed 's/\"//g') && echo MULTI_ACCOUNT: $MULTI_ACCOUNT && \
MULTI_SECRET=$(cat multi.json | jq .result.master_seed | sed 's/\"//g') && echo MULTI_SECRET: $MULTI_SECRET
```

### Sign and send trasfer from initial account (Sequence incremented)

```bash
SIGNED_BLOB=$(rippled sign $ISSUER_SECRET '{"TransactionType": "Payment", "Account": "'$ISSUER_ADDRESS'", "Destination": "'$MULTI_ACCOUNT'", "Amount": "1000000000", "Sequence": 4, "Fee": "10"}' offline | jq .result.tx_blob  | sed 's/\"//g') && \
rippled submit $SIGNED_BLOB
```

### Multisigning account info

```bash
rippled account_info $MULTI_ACCOUNT
```

### Setting up accounts that will sign transactions on behalf of miltisignature account

```bash
RESULT=$(rippled submit $MULTI_SECRET '{
    "Flags": 0,
    "TransactionType": "SignerListSet",
    "Account": "'$MULTI_ACCOUNT'",
    "Fee": "10000",
    "SignerQuorum": 2,
    "SignerEntries": [
        {
            "SignerEntry": {
                "Account": "'$ALICE_ACCOUNT'",
                "SignerWeight": 1
            }
        },
        {
            "SignerEntry": {
                "Account": "'$BOB_ACCOUNT'",
                "SignerWeight": 1
            }
        }
    ]
}') && \
echo $RESULT && \
echo $RESULT | jq .result.engine_result
```

### Accepting last transactions

```bash
rippled ledger_accept
```

### Check associations with Alice and Bob accounts

```bash
rippled account_objects $MULTI_ACCOUNT validated
```

### Create transaction

```bash
TX='{"TransactionType": "Payment", "Account": "'$MULTI_ACCOUNT'", "Destination": "'$CAROL_ACCOUNT'", "Amount": "300000000", "Sequence": 2, "Fee": "30000"}' && \
echo TX: $TX
```

### Sign by Alice

```bash
SIGNED_BY_ALICE=$(rippled sign_for $ALICE_ACCOUNT $ALICE_SECRET "$TX" offline) && \
echo SIGNED_BY_ALICE: $SIGNED_BY_ALICE && \
echo STATUS: $(echo $SIGNED_BY_ALICE | jq .result.status) && \
JSON_SIGNED_BY_ALICE=$(echo $SIGNED_BY_ALICE | jq .result.tx_json) && \
echo JSON_SIGNED_BY_ALICE: $JSON_SIGNED_BY_ALICE
```

### Add Bob signature

```bash
SIGNED_BY_ALICE_AND_BOB=$(rippled sign_for $BOB_ACCOUNT $BOB_SECRET "$JSON_SIGNED_BY_ALICE" offline) && \
echo SIGNED_BY_ALICE_AND_BOB: $SIGNED_BY_ALICE_AND_BOB && \
echo STATUS: $(echo $SIGNED_BY_ALICE_AND_BOB | jq .result.status) && \
JSON_SIGNED_BY_ALICE_AND_BOB=$(echo $SIGNED_BY_ALICE_AND_BOB | jq .result.tx_json) && \
echo JSON_SIGNED_BY_ALICE_AND_BOB: $JSON_SIGNED_BY_ALICE_AND_BOB
```

### Sumbit

```bash
RESULT=$(rippled submit_multisigned "$JSON_SIGNED_BY_ALICE_AND_BOB") && \
echo RESULT: $RESULT && \
echo STATUS: $(echo $RESULT | jq .result.status)
```

### Check Carol balance after sending multisignature transaction

```bash
CAROL_INFO=$(rippled account_info $CAROL_ACCOUNT) && \
echo CAROL_INFO: $CAROL_INFO && \
echo CAROL BALANCE: $(echo $CAROL_INFO | jq .result.account_data.Balance)
```
