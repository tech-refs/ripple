
# Escrow

## Time-Held Escrow

**[Setup standalone Alice $ Co](003-Init-Alice,-Bob-and-Carol-accounts.md)**

### Accepting last transactions

```bash
rippled ledger_accept
```

### Check balance

```bash
rippled account_info $BOB_ACCOUNT
```

### Create escrow with 1 min release time (online signing don't need put "sequence" and "fee")

```bash
RELEASE_TIME=$(expr $(date +%s) - 946684800 + 60) && \
RESULT=$(rippled submit $ALICE_SECRET '{
    "Account": "'$ALICE_ACCOUNT'",
    "TransactionType": "EscrowCreate",
    "Amount": "10000",
    "Destination": "'$BOB_ACCOUNT'",
    "FinishAfter": '$RELEASE_TIME'
}') && \
echo $RESULT && \
HASH=$(echo $RESULT | jq .result.tx_json.hash | sed 's/\"//g') && echo $HASH && \
SEQUENCE=$(echo $RESULT | jq .result.tx_json.Sequence) && echo $SEQUENCE
```

### Accepting last transactions

```bash
rippled ledger_accept
```
### Finish escrow after time

```bash
RESULT=$(rippled submit $ALICE_SECRET '{
    "Account": "'$ALICE_ACCOUNT'",
    "TransactionType": "EscrowFinish",
    "Owner": "'$ALICE_ACCOUNT'",
    "OfferSequence": '$SEQUENCE'
}') && \
echo $RESULT && \
HASH=$(echo $RESULT | jq .result.tx_json.hash | sed 's/\"//g') && echo $HASH
```

### Accepting last transaction

```bash
rippled ledger_accept
```

### Check new balance

```bash
rippled account_info $BOB_ACCOUNT
```

## Conditionally-Held Escrow

TODO