
# Rippled installation

### Install yum-utils and alien
```bash
sudo apt-get update && \
sudo apt-get install -y yum-utils alien
```

### Install the Ripple RPM repository
```bash
sudo rpm -Uvh https://mirrors.ripple.com/ripple-repo-el7.rpm
```

### Download the rippled software package
```bash
yumdownloader --enablerepo=ripple-stable --releasever=el7 rippled
```

### Verify the signature on the rippled software package
```bash
sudo rpm --import https://mirrors.ripple.com/rpm/RPM-GPG-KEY-ripple-release && rpm -K rippled*.rpm
```

### Install the rippled software package
```bash
sudo alien -i --scripts rippled*.rpm && rm rippled*.rpm
```

### Addings symbolic link to bin
```bash
sudo ln -s /opt/ripple/bin/rippled /bin/rippled
```

# Check

### Run standalone
```bash
sudo rippled -a
```

### Server info
```bash
rippled server_info
```
